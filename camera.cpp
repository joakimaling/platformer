#include "camera.hpp"
//----------------------------------------------------------------------------
Camera::Camera() {

}
//----------------------------------------------------------------------------
Camera::~Camera() {

}
//----------------------------------------------------------------------------
void Camera::init() {
	camera_x = 0;
	camera_y = 0;
}
//----------------------------------------------------------------------------
void Camera::tick(int x, int y, Map &map) {
	set_position(x, y, map);
}
//----------------------------------------------------------------------------
void Camera::draw(ALLEGRO_BITMAP *dest, ALLEGRO_BITMAP *face) {
	masked_blit(face, dest, 0, 0, camera_x + 10, camera_y + 10, face->w, face->h);
	blit(dest, screen, camera_x, camera_y, 0, 0, SCREEN_W, SCREEN_H);
}
//----------------------------------------------------------------------------
void Camera::set_position(int x, int y, Map &map) {
	camera_x = x - SCREEN_W / 2;
	camera_y = 0;
	if (camera_x < 0) {
		camera_x = 0;
	}
	else if (camera_x > map.size_x * TILE_SIZE - SCREEN_W) {
		camera_x = map.size_x * TILE_SIZE - SCREEN_W;
	}
}
//----------------------------------------------------------------------------
void Camera::debug(ALLEGRO_BITMAP *dest, Player &player) {
	char dir;
	textprintf_right_ex(dest, font, camera_x + SCREEN_W - 10, camera_y + 10, makecol(128, 128, 128), -1, "x: %d y: %d", player.x, player.y);
	textprintf_right_ex(dest, font, camera_x + SCREEN_W - 10, camera_y + 20, makecol(128, 128, 128), -1, "w: %d h: %d", player.w, player.h);
	textprintf_right_ex(dest, font, camera_x + SCREEN_W - 10, camera_y + 30, makecol(128, 128, 128), -1, "vx: %d vy: %d", player.vx, player.vy);
	textprintf_right_ex(dest, font, camera_x + SCREEN_W - 10, camera_y + 40, makecol(128, 128, 128), -1, "h_speed: %d v_speed: %d", player.h_speed, player.v_speed);
	if (player.h_dir == LEFT) {
		dir = 'L';
	}
	else if (player.h_dir == RIGHT) {
		dir = 'R';
	}
	else {
		dir = 'N';
	}
	textprintf_right_ex(dest, font, camera_x + SCREEN_W - 10, camera_y + 50, makecol(128, 128, 128), -1, "h_dir: %c", dir);
	if (player.v_dir == UP) {
		dir = 'U';
	}
	else if (player.v_dir == DOWN) {
		dir = 'D';
	}
	else {
		dir = 'N';
	}
	textprintf_right_ex(dest, font, camera_x + SCREEN_W - 10, camera_y + 60, makecol(128, 128, 128), -1, "v_dir: %c", dir);
	textprintf_right_ex(dest, font, camera_x + SCREEN_W - 10, camera_y + 70, makecol(128, 128, 128), -1, "idle: %d on_ground: %d", player.idle, player.on_ground);
}

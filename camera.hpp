#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <allegro5/allegro.h>
#include "player.hpp"
#include "map.hpp"

class Camera {
	public:
		Camera();
		~Camera();

		void init();
		void tick(int x, int y, Map &map);
		void draw(ALLEGRO_BITMAP *dest, ALLEGRO_BITMAP *face);
		void debug(ALLEGRO_BITMAP *dest, Player &player);

	private:
		void set_position(int x, int y, Map &map);
		int camera_x, camera_y;
};

#endif

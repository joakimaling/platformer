#include "collision.hpp"

#include <iostream>
//----------------------------------------------------------------------------
Collision::Collision() {

}
//----------------------------------------------------------------------------
Collision::~Collision() {

}
//----------------------------------------------------------------------------
void Collision::init() {

}
//----------------------------------------------------------------------------
void Collision::tick(Map &map, Player &player) {
	check(map, player);
	boundaries(player, map);
}
//----------------------------------------------------------------------------
void Collision::check(Map &map, Player &player) {
	for (int x = 0; x < map.size_x; x++) {
		for (int y = 0; y < map.size_y; y++) {
			if (map.map_grid[x][y].solid) {
				if (is_collision(&player, x, y)) {
					if (player.h_dir == RIGHT && player.on_ground) {
						player.x -= player.h_speed;
					}
					else if (player.h_dir == LEFT && player.on_ground) {
						player.x += player.h_speed;
					}
					if (player.v_dir == DOWN && !player.on_ground && player.y - player.vy <= y * TILE_SIZE) {
						player.y = y * TILE_SIZE - player.h;
						player.vy = 0;
						player.on_ground = true;
					}
					else if (player.v_dir == UP) {
						player.vy = 0;
						player.on_ground = false;
					}
				}
				else {
//					player.on_ground = false;
				}
			}
		}
	}
}
//----------------------------------------------------------------------------
void Collision::boundaries(Player &player, Map &map) {
	if (player.x < 0) {
		player.x = 0;
	}
	else if (player.x > map.size_x * TILE_SIZE - player.w) {
		player.x = map.size_x * TILE_SIZE - player.w;
	}
}
//----------------------------------------------------------------------------
bool Collision::is_collision(Player *player, int x, int y) {
	return (player->x < x * TILE_SIZE + TILE_SIZE && player->x + player->w > x * TILE_SIZE
			&& player->y < y * TILE_SIZE + TILE_SIZE && player->y + player->h > y * TILE_SIZE);
}

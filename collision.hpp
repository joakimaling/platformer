#ifndef COLLISION_HPP
#define COLLISION_HPP

#include <allegro5/allegro.h>
#include "player.h"
#include "map.h"

class Collision {
public:
	Collision();
	~Collision();

	void init();
	void tick(Map &map, Player &player);

private:
	void check(Map &map, Player &player);
	void boundaries(Player &player, Map &map);
	bool is_collision(Player *player, int x, int y);
};

#endif

﻿#include "game.hpp"
//----------------------------------------------------------------------------
Game::Game() {

}
//----------------------------------------------------------------------------
Game::~Game() {

}
//----------------------------------------------------------------------------
void Game::init() {
	map.init();
	player.init();
	camera.init();
	collision.init();
}
//----------------------------------------------------------------------------
void Game::tick() {
	map.tick();
	player.tick();
	collision.tick(map, player);
	camera.tick(player.x, player.y, map);
}
//----------------------------------------------------------------------------
void Game::draw(BITMAP *dest) {
	map.draw(dest);
	player.draw(dest);
	camera.debug(dest, player);
	camera.draw(dest, player.face);
}

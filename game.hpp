﻿#ifndef GAME_HPP
#define GAME_HPP

#include <allegro5/allegro.h>
#include "player.h"
#include "camera.h"
#include "collision.hpp"
#include "map.h"

class Game {
	public:
		Game();
		~Game();

		void init();
		void tick();
		void draw(ALLEGRO_BITMAP *dest);

	private:
		Player player;
		Camera camera;
		Map map;
		Collision collision;
};

#endif

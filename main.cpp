#include <allegro5/allegro.h>
#include "game.hpp"

volatile long counter = 0;

void increment() {
	counter++;
}

int main() {
	al_init();
	al_install_keyboard();
	install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, "A");
	set_color_depth(32);
	set_gfx_mode(GFX_AUTODETECT_WINDOWED, 800, 600, 0, 0);
	LOCK_VARIABLE(counter);
	LOCK_FUNCTION(increment);
	install_int_ex(increment, BPS_TO_TIMER(100));

	ALLEGRO_BITMAP* buffer = create_bitmap(SCREEN_W * 8, SCREEN_H);
	bool done = false;

	Game game;
	game.init();

	while(!done) {
		while(counter > 0) {

			// Input
			if(key[KEY_ESC]) {
				done = true;
			}

			// Update
			game.update();
			counter--;
		}

		// Draw
		game.draw(buffer);
		clear_to_color(buffer, makecol(0, 128, 0));
	}
	destroy_bitmap(buffer);
	return EXIT_SUCCESS;
}

#include "map.hpp"
//----------------------------------------------------------------------------
Map::Map() {
	sheet = load_bitmap("gfx/blocks1.bmp", NULL);
	size_x = 0;
	size_y = 0;
}
//----------------------------------------------------------------------------
Map::~Map() {
	destroy_bitmap(sheet);
}
//----------------------------------------------------------------------------
void Map::init(){
	load_x = 0;
	load_y = 0;
	load("maps/map1.txt");
}
//----------------------------------------------------------------------------
void Map::tick() {

}
//----------------------------------------------------------------------------
void Map::draw(ALLEGRO_BITMAP *dest) {
	for(int x = 0; x < size_x; x++) {
		for(int y = 0; y < size_y; y++) {
			if(map_grid[x][y].type == '1') {
				blit(sheet, dest, 580, 36, x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
			}
			else {
				blit(sheet, dest, 206, 36, x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
			}
		}
	}
}
//----------------------------------------------------------------------------
void Map::load(const char *file) {
	std::ifstream openfile(file);
	if(openfile.is_open()) {
		openfile >> size_x >> size_y;
		while(!openfile.eof()) {
			openfile >> map_grid[load_x][load_y].type;
			map_grid[load_x][load_y].solid = (map_grid[load_x][load_y].type == '2');
			load_x++;
			if(load_x >= size_x) {
				load_x = 0;
				load_y++;
			}
		}
		load_x = 0;
		load_y = 0;
	}
}

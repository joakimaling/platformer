#ifndef MAP_HPP
#define MAP_HPP

#include <allegro5/allegro.h>
#include <fstream>

#define TILE_SIZE 31

struct Tile {
	char type;
	bool solid;
};

class Map {
	public:
		Map();
		~Map();

		void init();
		void tick();
		void draw(ALLEGRO_BITMAP *dest);

		Tile map_grid[100][100];
		int size_x, size_y;
	private:
		void load(const char *file);
		int load_x, load_y;
		ALLEGRO_BITMAP *sheet;
};

#endif

#include "player.hpp"
#include "map.hpp"
//----------------------------------------------------------------------------
Player::Player() {
	sheet = load_bitmap("gfx/roll_edit.bmp", NULL);
	roll = create_bitmap(66, 62);
	face = create_bitmap(24, 24);
}
//----------------------------------------------------------------------------
Player::~Player(){
	destroy_bitmap(sheet);
	destroy_bitmap(roll);
	destroy_bitmap(face);
}
//----------------------------------------------------------------------------
void Player::init(){
	blit(sheet, face, 630, 10, 0, 0, face->w, face->h);
	x = 70;
	y = 372;
	w = roll->w;
	h = roll->h;
	vx = 0;
	vy = 0;
	on_ground = true;
	idle = true;
	h_dir = RIGHT;
	v_dir = NONE;
	h_speed = 5;
	v_speed = -15;
	gravity = 1;
	loop = 0;
	delay = 0;
}
//----------------------------------------------------------------------------
void Player::tick(){
	controls();
	set_position();
}
//----------------------------------------------------------------------------
void Player::draw(ALLEGRO_BITMAP *dest) {
	if(idle) {
		blit(sheet, roll, loop * roll->w, 0, 0, 0, roll->w, roll->h);
		if(h_dir == LEFT) {
			draw_sprite_h_flip(dest, roll, x, y);
		}
		else {
			draw_sprite(dest, roll, x, y);
		}
		if(delay >= 22) {
			loop = ++loop % 9;
			delay = 0;
		}
	}
	else {
		blit(sheet, roll, loop * roll->w, roll->h, 0, 0, roll->w, roll->h);
		if(h_dir == LEFT) {
			draw_sprite_h_flip(dest, roll, x, y);
		}
		else {
			draw_sprite(dest, roll, x, y);
		}
		if(delay >= 8) {
			loop = ++loop % 6;
			delay = 0;
		}
	}
	delay++;
}
//----------------------------------------------------------------------------
void Player::controls() {
	if(key[KEY_RIGHT]) {
		vx = h_speed;
		idle = false;
		h_dir = RIGHT;
	}
	else if(key[KEY_LEFT]) {
		vx = -h_speed;
		idle = false;
		h_dir = LEFT;
	}
	else {
		vx = 0;
		idle = true;
	}
	if(key[KEY_SPACE] && on_ground) {
		vy = v_speed;
		on_ground = false;
		v_dir = UP;
	}
}
//----------------------------------------------------------------------------
void Player::set_position(){
	if(vy >= 0) {
        v_dir = DOWN;
    }
	if(!on_ground) {
		vy += gravity;
	}
	else {
		vy = 0;
	}
	x += vx;
	y += vy;
}

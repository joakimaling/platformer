#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <allegro5/allegro.h>

enum Dir {
	DOWN, LEFT, NONE, RIGHT, UP
};

class Player {
	public:
		Player();
		~Player();

		void init();
		void tick();
		void draw(ALLEGRO_BITMAP *dest);

		int x, y, w, h, vx, vy, h_speed, v_speed, gravity;
		Dir h_dir, v_dir;
		bool idle, on_ground, jumping, falling;
		ALLEGRO_BITMAP * face;

	private:
		void controls();
		void set_position();

		ALLEGRO_BITMAP *sheet, *roll;
		int loop, delay;
};

#endif
